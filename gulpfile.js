'use strict';

/*************************************
 * Plugins section
 ************************************/

const gulp = require('gulp'),
    htmlmin = require('gulp-htmlmin'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    rimraf = require('rimraf'),
    sass = require('gulp-sass')(require('node-sass')),//плагин компиляции scss
    prefixer = require('gulp-autoprefixer'),//плагин расстановки префиксов
    stripCssComments = require('gulp-strip-css-comments'),// убрать комментарии из кода
    rigger = require('gulp-rigger'),
    uglify = require('gulp-uglify');
const {watch} = require("browser-sync");

/*************************************
 * Parameters section
 ************************************/

const path = {
    build: { // пути для сборки проектов
        html: 'build/',
        scss: 'build/css/',
        js: 'build/js',
        assets: 'build/img/'
    },
    src: { // пути размещения исходных файлов проекта
        html: 'src/*.html',
        scss: 'src/scss/*.scss',
        js: 'src/js/*.js',
        css: 'src/css/reset.css',
        assets: 'src/img/**.*'
    },
    watch: { // пути файлов, за изменением которых мы хотим наблюдать
        html: 'src/*.html',
        scss: 'src/scss/**/*.scss',
        js: 'src/js/*.js'
    },
    clean: 'build/*', // путь очистки директории для сборки

};


/***********************
 * Tasks
 ***********************/

// очистка директории
gulp.task('clean', function (done) {
    rimraf(path.clean, done);
});
// сборка html
gulp.task('prod:html', function (done) {
    gulp.src(path.src.html)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(path.build.html))
    .pipe(reload({
        stream: true
    }));
    done();
});
// сборка стилей
gulp.task('prod:scss', function (done) {
    gulp.src(path.src.scss)
        .pipe(sass({
            outputStyle: "compressed",
            sourcemaps: false
        }))
        .pipe(prefixer({
            cascade: false,
            // browsers: ['last 5 versions'],
            remove: true
        }))
        .pipe(stripCssComments())
        .pipe(gulp.dest(path.build.scss));
    gulp.src(path.src.css)
        .pipe(stripCssComments())
        .pipe(gulp.dest(path.build.scss))
    .pipe(reload({
        stream: true
    }));
    done();
});
// сборка js
gulp.task('prod:js', function (done) {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
    .pipe(reload({
        stream: true
    }));
    done();
});
// перенос assets
gulp.task('assets', function (done) {
    gulp.src(path.src.assets)
        .pipe(gulp.dest(path.build.assets))
    done()
})
// сборка


/***********************
 * Run watchers
 ***********************/

gulp.task('watch', function (done) {
    gulp.watch(path.watch.html, gulp.series('prod:html'));
    gulp.watch(path.watch.scss, gulp.series('prod:scss'));
    gulp.watch(path.watch.js, gulp.series('prod:js'));
    done();
});

const config = {
    server: {
        baseDir: "build/", // base directory
        index: "index.html", // start page
    },
    tunnel: true, // tunnel
    //proxy: 'donate.local', // for php and xampp vhosts
    host: 'localhost',
    port: 7787,
    logPrefix: "WebDev"
};

gulp.task('webserver', function (done) {
    browserSync(config);
    done();
});


gulp.task('default', gulp.series('clean', gulp.series( 'prod:js', 'prod:scss', 'assets', 'prod:html'), 'watch', 'webserver'));